package fr.troisil.fp.cindy;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@ToString
@Getter
public class Personne {

    private String prenom;
    private int age;
}
