package fr.troisil.fp.cindy;

import lombok.*;

@AllArgsConstructor
@Getter
@ToString
// constructeur avec un argument
@RequiredArgsConstructor
public class Eleve {

    private String nom;
    @Setter
    private Double note;





}
