package fr.troisil.fp.cindy;

import fr.troisil.fp.cindy.functions.PersonneListFunction;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.System.out;
import static java.lang.System.setOut;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
@Slf4j
public class Main {
    public static void main(String[] args) {

        Path path = Paths.get("src/main/resources/Personnes.csv");

        try{


            Stream <String> lines = Files.lines(path).skip(1);

            log.info("liste des personnes ");

            PersonneListFunction personneListFunction= new PersonneListFunction();
            List<Personne> personnes = lines
                    .map( element -> personneListFunction.apply(element)  )
                    .filter(Objects::nonNull)
                    .toList();
            personnes.forEach( personne -> log.info("{}" ,personne) );

            log.info(" liste des personnes par age croissant");

            List<Personne> personneAgeCroissant = personnes.stream()
                    .sorted((personne1, personne2) -> Integer.compare(personne1.getAge(), personne2.getAge()))
                    .collect(Collectors.toList());

            personneAgeCroissant.forEach( personne -> log.info("{}" , personne ) );


            Comparator<Personne> comparateurParAgeDecroissant = Comparator.comparing(Personne::getAge);

            List<Personne> personnesTrieComparateur = personnes.stream()
                    .sorted(comparateurParAgeDecroissant)
                    .collect(Collectors.toList());

            log.info(" liste des personnes par age croissant avec comparator ");
            personnesTrieComparateur.forEach(personne -> log.info("{}" , personne ));

            Comparator<Personne> comparateurAgePrenom = Comparator
                    .comparing(Personne::getAge)
                    .reversed()
                    .thenComparing(Personne::getPrenom);

            // Définition du comparateur par âge décroissant puis par prénom croissant
            Comparator<Personne> comparateurPrenom = Comparator
                    .comparing(Personne::getPrenom);
            // Tri de la liste par âge décroissant puis par prénom croissant
            List<Personne> personnesAgeCPrenomD = personnes.stream()
                    .sorted(comparateurParAgeDecroissant)
                    .sorted(comparateurPrenom)
                    .collect(Collectors.toList());
            log.info(" liste des personnes par age decroissant puis par prenom croissant");
            personnesAgeCPrenomD.forEach(personne -> log.info("{}" , personne ));



        } catch(IOException ex){

            ex.printStackTrace();
        }




    }
}