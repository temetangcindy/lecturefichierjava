package fr.troisil.fp.cindy.functions;

import fr.troisil.fp.cindy.Eleve;
import lombok.extern.slf4j.Slf4j;

import java.util.function.Consumer;
@Slf4j
public class EleveConsumer implements Consumer<Eleve> {

    //Consumer prend l’élève et affiche nom et note
    @Override
    public void accept(Eleve eleve) {
        log.info("Premier eleve = {}", eleve);
    }

}
