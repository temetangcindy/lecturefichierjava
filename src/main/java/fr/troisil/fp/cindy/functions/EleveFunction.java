package fr.troisil.fp.cindy.functions;

import fr.troisil.fp.cindy.Eleve;
import lombok.extern.slf4j.Slf4j;

import java.util.function.Function;
@Slf4j
public class EleveFunction implements Function<Eleve,Double> {


//Function  java prend un élève et retourne sa note
    @Override
    public Double apply(Eleve eleve) {

        log.info("Note eleve: {}",eleve.getNote());

        return eleve.getNote();
    }
}
