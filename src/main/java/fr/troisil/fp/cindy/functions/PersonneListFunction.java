package fr.troisil.fp.cindy.functions;

import fr.troisil.fp.cindy.Personne;

import java.util.function.Function;

public class PersonneListFunction implements Function<String, Personne> {
    @Override
    public Personne apply(String s) {
        String[] value = s.split(",");
        if(value.length>0){
            String prenom = value[0].trim();
            int age = Integer.valueOf(value[1].trim());
            return new Personne(prenom, age);
        }
        else{
            return null;
        }
    }
}
