package fr.troisil.fp.cindy.functions;

import fr.troisil.fp.cindy.Eleve;
import lombok.extern.slf4j.Slf4j;

import java.util.Random;
import java.util.function.Supplier;
@Slf4j
public class EleveSupplier implements Supplier<Eleve> {

    //Supplier retourne le nom avec note aléatoire

    Random rand = new Random();
    @Override
    public Eleve get() {

    Eleve e = new Eleve("cindy",rand.nextDouble(0.00,20.00));
        log.info("Eleve avec note aleatoire: {}",e);

        return e  ;
    }
}

