package fr.troisil.fp.cindy.functions;

import fr.troisil.fp.cindy.Eleve;
import org.junit.jupiter.api.Test;


public class EleveConsumerTest {


    @Test
    public void testAccept() {

        //Arrange
        Eleve eleve = new Eleve("cindy",18.05);
        EleveConsumer elc= new EleveConsumer();

        //Act
        elc.accept(eleve);

    }


}
