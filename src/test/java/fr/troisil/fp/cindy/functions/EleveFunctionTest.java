package fr.troisil.fp.cindy.functions;

import fr.troisil.fp.cindy.Eleve;
import org.junit.jupiter.api.Test;
import java.lang.Double;


import static org.junit.jupiter.api.Assertions.assertEquals;


public class EleveFunctionTest {

    @Test
    public void  testApply() {

        //Arrange
        Eleve eleve = new Eleve("cindy",18.05);
        EleveFunction eleveFunction = new EleveFunction();

        //Act
        Double elvNote = eleveFunction.apply(eleve);

        //Assert
        assertEquals(18.05, elvNote);



    }

}
